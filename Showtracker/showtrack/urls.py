from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # Models
    url(r'^show/(?P<show_id>[0-9]+)/$', views.get_show, name='show'),
    url(r'^episode/(?P<episode_id>[0-9]+)/$', views.get_episode, name='episode'),
    url(r'^network/(?P<network_id>[0-9]+)/$', views.get_network, name='network'),
    url(r'^cast/(?P<cast_id>[0-9]+)/$', views.get_cast, name='cast'),

    # Delete
    url(r'^delete_show/(?P<show_id>[0-9]+)/$', views.delete_show, name="delete_show"),
    url(r'^delete_episode/(?P<episode_id>[0-9]+)/$', views.delete_episode, name="delete_episode"),
    url(r'^delete_cast/(?P<cast_id>[0-9]+)/$', views.delete_cast, name="delete_cast"),
    url(r'^delete_network/(?P<network_id>[0-9]+)/$', views.delete_network, name="delete_network"),

    # Modify
    url(r'^modify_show/(?P<show_id>[0-9]+)/$', views.modify_show, name="modify_show"),
    url(r'^modify_episode/(?P<episode_id>[0-9]+)/$', views.modify_episode, name="modify_episode"),
    url(r'^modify_cast/(?P<cast_id>[0-9]+)/$', views.modify_cast, name="modify_cast"),
    url(r'^modify_network/(?P<network_id>[0-9]+)/$', views.modify_network, name="modify_network"),

    # All models
    url(r'^shows', views.get_all_shows, name='shows'),
    url(r'^networks', views.get_all_networks, name='networks'),
    url(r'^casts', views.get_all_cast, name='casts'),

    # Dashboard
    url(r'^overview', views.overview, name='overview'),
    url(r'^table_show', views.table_show, name='table_show'),
    url(r'^table_episode', views.table_episode, name='table_episode'),
    url(r'^table_cast', views.table_cast, name='table_cast'),
    url(r'^table_network', views.table_network, name='table_network'),
    url(r'^add_cast_show', views.add_cast_show, name='add_cast_show'),
    url(r'^add_cast', views.add_cast, name='add_cast'),

    # Forms
    url(r'^new_show', views.new_show, name='new_show'),
    url(r'^create_show', views.create_show, name='create_show'),
    url(r'^edit_show/(?P<show_id>[0-9]+)/$', views.edit_show, name='edit_show'),
    url(r'^new_episode', views.new_episode, name='new_episode'),
    url(r'^create_episode', views.create_episode, name='create_episode'),
    url(r'^edit_episode/(?P<episode_id>[0-9]+)/$', views.edit_episode, name='edit_episode'),
    url(r'^new_cast', views.new_cast, name='new_cast'),
    url(r'^create_cast', views.create_cast, name='create_cast'),
    url(r'^edit_cast/(?P<cast_id>[0-9]+)/$', views.edit_cast, name='edit_cast'),
    url(r'^new_network', views.new_network, name='new_network'),
    url(r'^create_network', views.create_network, name='create_network'),
    url(r'^edit_network/(?P<network_id>[0-9]+)/$', views.edit_network, name='edit_network'),

    # Registration
    url(r'^signin', views.sign_in, name='signin'),
    url(r'^login', views.login_view, name='login'),
    url(r'^logout', views.logout_view, name='logout'),

]
