from django import forms
from .models import Show, Episode, Cast, Network


class ShowForm(forms.ModelForm):
    class Meta:
        fields = ('id', 'name', 'photo', 'seasons', 'synopsis', 'genre', 'year', 'duration', 'finished', 'trailer', 'network')
        model = Show


class EpisodeForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'number', 'season', 'synopsis', 'aired', 'photo', 'show')
        model = Episode


class CastForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'biography', 'birthday', 'role', 'photo')
        model = Cast


class NetworkForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'country', 'founded', 'website', 'language', 'photo')
        model = Network
