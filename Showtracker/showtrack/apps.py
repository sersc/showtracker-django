from django.apps import AppConfig


class ShowtrackConfig(AppConfig):
    name = 'showtrack'
