from django.db import models
from datetime import date


def show_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/shows/<shows>/<filename>
    return 'img/shows/{0}/{1}'.format(instance.name, filename)


def episode_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/shows/<shows>/<season>/<filename>
    return 'img/shows/{0}/season_{1}/{2}'.format(instance.show.name, instance.season, filename)


def cast_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/cast/<filename>
    return 'img/cast/{0}'.format(filename)


def network_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/img/network/<filename>
    return 'img/network/{0}'.format(filename)


class Show(models.Model):
    name = models.CharField(max_length=255)
    photo = models.ImageField(upload_to=show_path, default='')
    seasons = models.IntegerField(default=1)
    synopsis = models.TextField(default='')
    genre = models.CharField(max_length=255)
    year = models.CharField(max_length=255)
    duration = models.IntegerField(default=40)
    finished = models.BooleanField(default=False)
    trailer = models.CharField(max_length=255)
    network = models.ForeignKey('Network', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.name


class Episode(models.Model):
    name = models.CharField(max_length=255)
    number = models.IntegerField(default=0)
    season = models.IntegerField(default=0)
    synopsis = models.TextField(default='')
    aired = models.DateField(null=True, default=date.today())
    photo = models.ImageField(upload_to=episode_path, default='')
    show = models.ForeignKey('Show', on_delete=models.CASCADE)

    def __str__(self):
        return self.show.name + " - s" + str(self.season) + "e" + str(self.number)


class Cast(models.Model):
    name = models.CharField(max_length=255)
    biography = models.TextField(default='')
    birthday = models.DateField(null=True, default=date.today())
    role = models.CharField(max_length=255)
    photo = models.ImageField(upload_to=cast_path, default='')
    shows = models.ManyToManyField(Show, through='CastShow')

    def __str__(self):
        return self.name


class Network(models.Model):
    name = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    founded = models.DateField(null=True, default=date.today())
    website = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    photo = models.ImageField(upload_to=network_path, default='')

    def __str__(self):
        return self.name


# Intermediate Cast-Show
class CastShow(models.Model):
    cast = models.ForeignKey(Cast, on_delete=models.CASCADE)
    show = models.ForeignKey(Show, on_delete=models.CASCADE)

