from django.contrib import admin
from .models import Show, Episode, Cast, Network

admin.site.register(Show)
admin.site.register(Episode)
admin.site.register(Cast)
admin.site.register(Network)
