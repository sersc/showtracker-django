from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.contrib import messages
from .models import Show, Episode, Cast, Network, CastShow
from .forms import ShowForm, EpisodeForm, CastForm, NetworkForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from Showtracker.settings import RESULTS_PER_PAGE

import json


def index(request):
    return render(request, 'showtrack/index.html')


# ============= GET ONE OBJECT ================= #


def get_show(request, show_id):
    try:
        show = Show.objects.get(pk=show_id)
        episodes = Episode.objects.filter(show__exact=show_id)
        cast = Cast.objects.filter(shows__exact=show_id)
    except Show.DoesNotExist:
        raise Http404("That shows does not exist.")

    context = {'show': show, 'episodes': episodes, 'cast': cast}
    return render(request, 'showtrack/show.html', context)


def get_episode(request, episode_id):
    try:
        episode = Episode.objects.get(pk=episode_id)
    except Episode.DoesNotExist:
        raise Http404("That episode does not exist.")

    context = {'episode': episode}
    return render(request, 'showtrack/episode.html', context)


def get_cast(request, cast_id):
    try:
        cast = Cast.objects.get(pk=cast_id)
        shows = cast.shows.all()
    except Cast.DoesNotExist:
        raise Http404("That person does not exist.")

    context = {'cast': cast, 'shows': shows}
    return render(request, 'showtrack/cast.html', context)


def get_network(request, network_id):
    try:
        network = Network.objects.get(pk=network_id)
        shows = Show.objects.filter(network__exact=network_id)
    except Network.DoesNotExist:
        raise Http404("That network does not exist.")

    context = {'network': network, 'shows': shows}
    return render(request, 'showtrack/network.html', context)


# ============= GET ALL OBJECTS ================= #


def get_all_shows(request):
    try:
        search = request.GET.get('search', None)
        if search is not None:
            all_shows = Show.objects.filter(name__contains=search)
        else:
            all_shows = Show.objects.all()
        page = request.GET.get('page', 1)

        paginator = Paginator(all_shows, RESULTS_PER_PAGE)
        try:
            shows = paginator.page(page)
        except PageNotAnInteger:
            shows = paginator.page(1)
        except EmptyPage:
            shows = paginator.page(paginator.num_pages)
    except Show.DoesNotExist:
        raise Http404("No shows founded")

    context = {'shows': shows}
    return render(request, 'showtrack/elements/shows.html', context)


def get_all_cast(request):
    try:
        search = request.GET.get('search', None)
        if search is not None:
            all_casts = Cast.objects.filter(name__contains=search)
        else:
            all_casts = Cast.objects.all()
        page = request.GET.get('page', 1)

        paginator = Paginator(all_casts, RESULTS_PER_PAGE)
        try:
            casts = paginator.page(page)
        except PageNotAnInteger:
            casts = paginator.page(1)
        except EmptyPage:
            casts = paginator.page(paginator.num_pages)
    except Show.DoesNotExist:
        raise Http404("No cast found")

    context = {'casts': casts}
    return render(request, 'showtrack/elements/casts.html', context)


def get_all_networks(request):
    try:
        search = request.GET.get('search', None)
        if search is not None:
            all_networks = Network.objects.filter(name__contains=search)
        else:
            all_networks = Network.objects.all()
        page = request.GET.get('page', 1)

        paginator = Paginator(all_networks, RESULTS_PER_PAGE)
        try:
            networks = paginator.page(page)
        except PageNotAnInteger:
            networks = paginator.page(1)
        except EmptyPage:
            networks = paginator.page(paginator.num_pages)
    except Network.DoesNotExist:
        raise Http404("No network found")

    context = {'networks': networks}
    return render(request, 'showtrack/elements/networks.html', context)


# ============= DELETE VIEWS ================= #


def delete_show(request, show_id):
    if request.user.is_authenticated:
        show = Show.objects.get(pk=show_id)
        show.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin')


def delete_episode(request, episode_id):
    if request.user.is_authenticated:
        episode = Episode.objects.get(pk=episode_id)
        episode.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin')


def delete_cast(request, cast_id):
    if request.user.is_authenticated:
        cast = Cast.objects.get(pk=cast_id)
        cast.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin')


def delete_network(request, network_id):
    if request.user.is_authenticated:
        network = Network.objects.get(pk=network_id)
        network.delete()
        return HttpResponse(json.dumps({}), content_type='application/json')
    else:
        return redirect('signin')


# ============= MODIFY VIEWS ================= #


def modify_show(request, show_id):
    if request.user.is_authenticated:
        show = Show.objects.get(pk=show_id)
        networks = Network.objects.all()
        context = {'show': show, 'networks': networks}
        return render(request, 'showtrack/forms/new_show.html', context)
    else:
        return redirect('signin')


def modify_episode(request, episode_id):
    if request.user.is_authenticated:
        episode = Episode.objects.get(pk=episode_id)
        shows = Show.objects.all()
        context = {'episode': episode, 'shows': shows}
        return render(request, 'showtrack/forms/new_episode.html', context)
    else:
        return redirect('signin')


def modify_cast(request, cast_id):
    if request.user.is_authenticated:
        cast = Cast.objects.get(pk=cast_id)
        context = {'cast': cast}
        return render(request, 'showtrack/forms/new_cast.html', context)
    else:
        return redirect('signin')


def modify_network(request, network_id):
    if request.user.is_authenticated:
        network = Network.objects.get(pk=network_id)
        context = {'network': network}
        return render(request, 'showtrack/forms/new_network.html', context)
    else:
        return redirect('signin')


# ============= FORM VIEWS ================= #


def new_show(request):
    if request.user.is_authenticated:
        networks = Network.objects.all()
        form = ShowForm()
        context = {'form': form, 'networks': networks}
        return render(request, 'showtrack/forms/new_show.html', context)
    else:
        return redirect('signin')


def create_show(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ShowForm(request.POST, request.FILES)
            if form.is_valid():
                show = Show()
                show.name = form.cleaned_data['name']
                show.photo = form.cleaned_data['photo']
                show.seasons = form.cleaned_data['seasons']
                show.synopsis = form.cleaned_data['synopsis']
                show.genre = form.cleaned_data['genre']
                show.year = form.cleaned_data['year']
                show.duration = form.cleaned_data['duration']
                show.finished = form.cleaned_data['finished']
                show.trailer = form.cleaned_data['trailer']
                show.network = form.cleaned_data['network']
                show.save()
            else:
                return render(request, 'showtrack/forms/new_show.html', {'form': form})

            messages.success(request, 'Show created successfully')
            return redirect('new_show')
    else:
        return redirect('signin')


def edit_show(request, show_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ShowForm(request.POST, request.FILES)
            if form.is_valid():
                show = Show()
                show.id = show_id
                show.name = form.cleaned_data['name']
                show.photo = form.cleaned_data['photo']
                show.seasons = form.cleaned_data['seasons']
                show.synopsis = form.cleaned_data['synopsis']
                show.genre = form.cleaned_data['genre']
                show.year = form.cleaned_data['year']
                show.duration = form.cleaned_data['duration']
                show.finished = form.cleaned_data['finished']
                show.trailer = form.cleaned_data['trailer']
                show.network = form.cleaned_data['network']
                show.save()
            else:
                return render(request, 'showtrack/forms/new_show.html', {'form': form})

            messages.success(request, 'Show edited successfully')
            return redirect('new_show')
    else:
        return redirect('signin')


def new_episode(request):
    if request.user.is_authenticated:
        form = EpisodeForm()
        shows = Show.objects.all()
        context = {'form': form, 'shows': shows}
        return render(request, 'showtrack/forms/new_episode.html', context)
    else:
        return redirect('signin')


def create_episode(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = EpisodeForm(request.POST, request.FILES)
            if form.is_valid():
                episode = Episode()
                episode.name = form.cleaned_data['name']
                episode.season = form.cleaned_data['season']
                episode.number = form.cleaned_data['number']
                episode.synopsis = form.cleaned_data['synopsis']
                episode.aired = form.cleaned_data['aired']
                episode.photo = form.cleaned_data['photo']
                episode.show = form.cleaned_data['show']
                episode.save()
            else:
                return render(request, 'showtrack/forms/new_episode.html', {'form': form})

            messages.success(request, 'Episode created successfully')
            return redirect('new_episode')
    else:
        return redirect('signin')


def edit_episode(request, episode_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = EpisodeForm(request.POST, request.FILES)
            if form.is_valid():
                episode = Episode()
                episode.id = episode_id
                episode.name = form.cleaned_data['name']
                episode.season = form.cleaned_data['season']
                episode.number = form.cleaned_data['number']
                episode.synopsis = form.cleaned_data['synopsis']
                episode.aired = form.cleaned_data['aired']
                episode.photo = form.cleaned_data['photo']
                episode.show = form.cleaned_data['show']
                episode.save()
            else:
                return render(request, 'showtrack/forms/new_episode.html', {'form': form})

            messages.success(request, 'Episode edited successfully')
            return redirect('new_episode')
    else:
        return redirect('signin')


def new_cast(request):
    if request.user.is_authenticated:
        form = CastForm()
        context = {'form': form}
        return render(request, 'showtrack/forms/new_cast.html', context)
    else:
        return redirect('signin')


def create_cast(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CastForm(request.POST, request.FILES)
            if form.is_valid():
                cast = Cast()
                cast.name = form.cleaned_data['name']
                cast.biography = form.cleaned_data['biography']
                cast.birthday = form.cleaned_data['birthday']
                cast.role = form.cleaned_data['role']
                cast.photo = form.cleaned_data['photo']
                cast.save()
            else:
                return render(request, 'showtrack/forms/new_cast.html', {'form': form})

            messages.success(request, 'Cast created successfully')
            return redirect('new_cast')
    else:
        return redirect('signin')


def edit_cast(request, cast_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CastForm(request.POST, request.FILES)
            if form.is_valid():
                cast = Cast()
                cast.id = cast_id
                cast.name = form.cleaned_data['name']
                cast.biography = form.cleaned_data['biography']
                cast.birthday = form.cleaned_data['birthday']
                cast.role = form.cleaned_data['role']
                cast.photo = form.cleaned_data['photo']
                cast.save()
            else:
                return render(request, 'showtrack/forms/new_cast.html', {'form': form})

            messages.success(request, 'Cast edited successfully')
            return redirect('new_cast')
    else:
        return redirect('signin')


def new_network(request):
    if request.user.is_authenticated:
        form = NetworkForm()
        context = {'form': form}
        return render(request, 'showtrack/forms/new_network.html', context)
    else:
        return redirect('signin')


def create_network(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = NetworkForm(request.POST, request.FILES)
            if form.is_valid():
                network = Network()
                network.name = form.cleaned_data['name']
                network.country = form.cleaned_data['country']
                network.founded = form.cleaned_data['founded']
                network.website = form.cleaned_data['website']
                network.language = form.cleaned_data['language']
                network.photo = form.cleaned_data['photo']
                network.save()
            else:
                return render(request, 'showtrack/forms/new_network.html', {'form': form})

            messages.success(request, 'Network created successfully')
            return redirect('new_network')
    else:
        return redirect('signin')


def edit_network(request, network_id):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = NetworkForm(request.POST, request.FILES)
            if form.is_valid():
                network = Network()
                network.id = network_id
                network.name = form.cleaned_data['name']
                network.country = form.cleaned_data['country']
                network.founded = form.cleaned_data['founded']
                network.website = form.cleaned_data['website']
                network.language = form.cleaned_data['language']
                network.photo = form.cleaned_data['photo']
                network.save()
            else:
                return render(request, 'showtrack/forms/new_network.html', {'form': form})

            messages.success(request, 'Network edited successfully')
            return redirect('new_network')
    else:
        return redirect('signin')


# ============= DASHBOARD VIEWS ================= #


def overview(request):
    if request.user.is_authenticated:
        users = User.objects.count()
        shows = Show.objects.count()
        episodes = Episode.objects.count()
        cast = Cast.objects.count()
        network = Network.objects.count()

        context = {'users': users, 'shows': shows, 'episodes': episodes, 'cast': cast, 'network': network}
        return render(request, 'showtrack/userpages/overview.html', context)
    else:
        return redirect('signin')


def table_show(request):
    if request.user.is_authenticated:
        try:
            shows = Show.objects.all()
        except Show.DoesNotExist:
            raise Http404("No show found")

        context = {'shows': shows}
        return render(request, 'showtrack/userpages/table_show.html', context)
    else:
        return redirect('signin')


def table_episode(request):
    if request.user.is_authenticated:
        try:
            episodes = Episode.objects.all()
        except Episode.DoesNotExist:
            raise Http404("No episodes found")

        context = {'episodes': episodes}
        return render(request, 'showtrack/userpages/table_episode.html', context)
    else:
        return redirect('signin')


def table_cast(request):
    if request.user.is_authenticated:
        try:
            cast = Cast.objects.all()
        except Cast.DoesNotExist:
            raise Http404("No cast found")

        context = {'cast': cast}
        return render(request, 'showtrack/userpages/table_cast.html', context)
    else:
        return redirect('signin')


def table_network(request):
    if request.user.is_authenticated:
        try:
            networks = Network.objects.all()
        except Network.DoesNotExist:
            raise Http404("No network found")

        context = {'networks': networks}
        return render(request, 'showtrack/userpages/table_network.html', context)
    else:
        return redirect('signin')


def add_cast(request):
    if request.user.is_authenticated:
        cast = Cast.objects.get(pk=request.GET.get('cast', None))
        show = Show.objects.get(pk=request.GET.get('show', None))

        castshow = CastShow(cast=cast, show=show)
        castshow.save()

        return render(request, 'showtrack/userpages/table_cast.html')
    else:
        return redirect('signin')


def add_cast_show(request):
    if request.user.is_authenticated:
        shows = Show.objects.all()
        cast = Cast.objects.all()

        context = {'shows': shows, 'cast': cast}

        return render(request, 'showtrack/userpages/add_cast_show.html', context)
    else:
        return redirect('signin')

# ============= REGISTRATION VIEWS ================= #


def sign_in(request):
    return render(request, 'showtrack/registration/signin.html')


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('index')

    context = {'message': 'Invalid Username/Password'}
    return render(request, 'showtrack/registration/signin.html', context)


def logout_view(request):
    logout(request)
    return redirect('index')
