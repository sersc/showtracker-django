# ShowTracker #

This project gives you information about the details of the show, who acts in that show and which network produced the show.

### Getting Started ###

* Go to [ShowTracker](http://217.182.169.85/~ssoler/showtracker-django)
* Navigate though the home page and search for shows.

### Built With ###

* Django 1.11.8
* Bootstrap

### Authors ###

* Sergio Soler